// CPP_Window_WPF.cpp : Defines the entry point for the application.
//

#include "framework.h"
#include "CPP_Window_WPF.h"
//#import "..\MyButton\bin\Debug\MyButton.tlb" named_guids raw_interfaces_only
#import "..\MyButton\bin\Debug\MyButton.tlb" named_guids raw_interfaces_only

#define MAX_LOADSTRING 100

#define DOT_NET_BUTTON_PRESSED  0x0400
HWND _hAtl;
HWND _hSelf;
IUnknown* _pUnk;
MyButton::ICSSExplorerInterfacePtr _pDotNetCOMPtr;
HINSTANCE _hWebLib = ::LoadLibrary(TEXT("ATL.DLL"));
// Global Variables:
HINSTANCE hInst; // current instance
WCHAR szTitle[MAX_LOADSTRING]; // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING]; // the main window class name

// Forward declarations of functions included in this code module:
ATOM MyRegisterClass(HINSTANCE hInstance);
BOOL InitInstance(HINSTANCE, int);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK About(HWND, UINT, WPARAM, LPARAM);

void loadActiveX(LPCTSTR strActiveXName)
{
	BOOL (WINAPI * m_AtlAxWinInit)();
	m_AtlAxWinInit = (BOOL(WINAPI*)(void))::GetProcAddress(_hWebLib, "AtlAxWinInit");
	m_AtlAxWinInit();
	RECT rcClient;
	GetClientRect(_hSelf, &rcClient);
	_hAtl = ::CreateWindowEx(
		WS_EX_CLIENTEDGE,
		TEXT("AtlAxWin"),
		strActiveXName,
		WS_CHILD | WS_VISIBLE | /*WS_CLIPCHILDREN | */WS_EX_RTLREADING,
		0, 0, rcClient.right, rcClient.bottom,
		_hSelf,
		NULL,
		NULL,
		NULL);

	if (!_hAtl)
	{
		MessageBox(NULL, TEXT("Can not load AtlAxWin!"),
		           szTitle, MB_OK | MB_ICONSTOP);
		throw int(106901);
	}

	HRESULT (WINAPI * m_AtlAxGetControl)(HWND h, IUnknown* * pp);
	m_AtlAxGetControl = (HRESULT(WINAPI*)
		(HWND, IUnknown* *))::GetProcAddress(_hWebLib, "AtlAxGetControl");
	m_AtlAxGetControl(_hAtl, &_pUnk);
	_pUnk->QueryInterface(__uuidof(MyButton::ICSSExplorerInterface),
	                      (LPVOID*)& _pDotNetCOMPtr);
	if (_pDotNetCOMPtr != NULL)
	{
		_pDotNetCOMPtr->setAdapterDllPtr((long)_hSelf);
	}
	else
	{
		RECT rcClient;
		GetClientRect(_hSelf, &rcClient);

		::DestroyWindow(_hAtl);
		_hAtl = ::CreateWindowEx(
			WS_EX_CLIENTEDGE,
			TEXT("AtlAxWin"),
			TEXT("MSHTML:""Please register ActiveX control before using this plugin."""),
			WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN |
			WS_EX_RTLREADING,
			0, 0, rcClient.right, rcClient.bottom,
			_hSelf,
			NULL,
			NULL,
			NULL);
	}
}

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                      _In_opt_ HINSTANCE hPrevInstance,
                      _In_ LPWSTR lpCmdLine,
                      _In_ int nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.

	// Initialize global strings
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_CPPWINDOWWPF, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_CPPWINDOWWPF));

	MSG msg;

	// Main message loop:
	loadActiveX(L"MyButton.MyButton");
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}


//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_CPPWINDOWWPF));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_CPPWINDOWWPF);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable

	HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
	                          CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	_hSelf = hWnd;
	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE: Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	const char* strHelloWorld = "Hello World!";
	const char* strOK = "OK";
	_bstr_t bstrHelloWorld(strHelloWorld);
	_bstr_t bstrOK(strOK);
	switch (message)
	{
	case WM_COMMAND:
		{
			int wmId = LOWORD(wParam);
			// Parse the menu selections:
			switch (wmId)
			{
			case IDM_ABOUT:
				DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
				break;
			case IDM_EXIT:
				DestroyWindow(hWnd);
				break;
			case IDM_BTN_CAP_HELLOWORLD:
				_pDotNetCOMPtr->setButtonCaption(bstrHelloWorld);
				break;
			case IDM_BTN_CAP_OK:
				_pDotNetCOMPtr->setButtonCaption(bstrOK);
				break;
			default:
				return DefWindowProc(hWnd, message, wParam, lParam);
			}
		}
		break;
	case WM_PAINT:
		{
			PAINTSTRUCT ps;
			HDC hdc = BeginPaint(hWnd, &ps);
			// TODO: Add any drawing code that uses hdc here...
			EndPaint(hWnd, &ps);
		}
		break;
	case WM_DESTROY:
		_pDotNetCOMPtr->Release();
		::DestroyWindow(_hAtl);
		_pUnk->Release();
		::FreeLibrary(_hWebLib);
		PostQuitMessage(0);
		break;
	case DOT_NET_BUTTON_PRESSED:
		MessageBox(NULL, TEXT("Message from C# arrived: Button Pressed!!"), szTitle, MB_OK | MB_ICONINFORMATION);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
