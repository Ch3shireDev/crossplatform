﻿using System;

namespace MyButton
{
    partial class MyButton
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        void MyButtonControl_Click(object sender, EventArgs e)
        {
            SendMessage(m_AdapterDllPtr.ToInt32(), DOT_NET_BUTTON_PRESSED, IntPtr.Zero, IntPtr.Zero);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MyButtonControl = new System.Windows.Forms.Button();
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            this.elementHost2 = new System.Windows.Forms.Integration.ElementHost();
            this.myButton1 = new MyButtonWPF.MyButton();
            this.SuspendLayout();
            // 
            // MyButtonControl
            // 
            this.MyButtonControl.Location = new System.Drawing.Point(3, 3);
            this.MyButtonControl.Name = "MyButtonControl";
            this.MyButtonControl.Size = new System.Drawing.Size(75, 23);
            this.MyButtonControl.TabIndex = 0;
            this.MyButtonControl.Text = "Press Me";
            this.MyButtonControl.UseVisualStyleBackColor = true;
            this.MyButtonControl.Click += new System.EventHandler(this.MyButtonControl_Click);
            // 
            // elementHost1
            // 
            this.elementHost1.Location = new System.Drawing.Point(18, 32);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(587, 432);
            this.elementHost1.TabIndex = 1;
            this.elementHost1.Text = "elementHost1";
            this.elementHost1.Child = null;
            // 
            // elementHost2
            // 
            this.elementHost2.Location = new System.Drawing.Point(18, 32);
            this.elementHost2.Name = "elementHost2";
            this.elementHost2.Size = new System.Drawing.Size(587, 432);
            this.elementHost2.TabIndex = 2;
            this.elementHost2.Text = "elementHost2";
            this.elementHost2.Child = this.myButton1;
            // 
            // MyButton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.elementHost2);
            this.Controls.Add(this.elementHost1);
            this.Controls.Add(this.MyButtonControl);
            this.Name = "MyButton";
            this.Size = new System.Drawing.Size(630, 480);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button MyButtonControl;
        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private System.Windows.Forms.Integration.ElementHost elementHost2;
        private MyButtonWPF.MyButton myButton1;
    }
}
