﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;

namespace MyButton
{
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface ICSSExplorerInterface
    {
        void setButtonCaption(string strNewCaption);
        void setAdapterDllPtr(IntPtr i_AdapterDllPtr);
    }

    [ProgId("MyButton.MyButton")]
    [ClassInterface(ClassInterfaceType.None)]
    public partial class MyButton : UserControl, ICSSExplorerInterface
    {
        private static uint DOT_NET_BUTTON_PRESSED = 0x0400;
        private IntPtr m_AdapterDllPtr;

        public MyButton()
        {
            InitializeComponent();
        }

        public void setButtonCaption(string strNewCaption)
        {
            MyButtonControl.Text = strNewCaption;
        }

        public void setAdapterDllPtr(IntPtr i_AdapterDllPtr)
        {
            m_AdapterDllPtr = i_AdapterDllPtr;
        }

        [ComRegisterFunction()]
        public static void RegisterClass(string i_Key)
        {
            var sb = new StringBuilder(i_Key);
            sb.Replace(@"HKEY_CLASSES_ROOT\", "");
            var registerKey = Registry.ClassesRoot.OpenSubKey(sb.ToString(), true);
            var ctrl = registerKey.CreateSubKey("Control");
            ctrl.Close();
            var inprocServer32 = registerKey.OpenSubKey("InprocServer32", true);
            inprocServer32.SetValue("CodeBase", Assembly.GetExecutingAssembly().CodeBase);
            inprocServer32.Close();
            registerKey.Close();
        }

        [ComUnregisterFunction()]
        public static void UnregisterClass(string i_Key)
        {
            var sb = new StringBuilder(i_Key);
            sb.Replace(@"HKEY_CLASSES_ROOT\", "");
            var registerKey = Registry.ClassesRoot.OpenSubKey(sb.ToString(), true);
            registerKey.DeleteSubKey("Control", false);
            var inprocServer32 = registerKey.OpenSubKey("InprocServer32", true);
            inprocServer32.DeleteSubKey("CodeBase", false);
            registerKey.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            SendMessage(m_AdapterDllPtr.ToInt32(), DOT_NET_BUTTON_PRESSED, IntPtr.Zero, IntPtr.Zero);
        }

        #region MAPPING_OF_USER32_DLL_SECTION

        [DllImport("user32.dll", EntryPoint = "SendMessage")]
        public static extern IntPtr SendMessage(
            int hwnd, uint wMsg, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll", EntryPoint = "SendMessage")]
        public static extern int SendMessage(
            int hwnd, uint wMsg, int wParam, string lParam);

        [DllImport("user32.dll", EntryPoint = "SendMessage")]
        public static extern int SendMessage(
            int hwnd, uint wMsg, int wParam, out int lParam);

        [DllImport("user32.dll", EntryPoint = "SendMessage")]
        public static extern int GetNbFiles(
            int hwnd, uint wMsg, int wParam, int lParam);

        [DllImport("user32.dll", EntryPoint = "SendMessage")]
        public static extern int GetFileNames(
            int hwnd, uint wMsg,
            [MarshalAs(UnmanagedType.LPArray)] IntPtr[] wParam,
            int lParam);

        [DllImport("user32.dll", EntryPoint = "SendMessage")]
        public static extern int SendMessage(
            int hwnd, uint wMsg, int wParam, StringBuilder lParam);

        #endregion
    }
}