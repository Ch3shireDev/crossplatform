# Crossplatform WPF

From the article https://www.codeproject.com/Articles/42965/How-to-Integrate-C-Window-in-C-Project .

# Kontrolka WinForms

- Stwórz nowy projekt Windows Forms Control Library.
- Wewnątrz Assembly Information zaznacz Make Assembly COM-Visible.
- Wewnątrz zakładki Build zaznacz `Register for COM interop` oraz `Generate serialization assembly: On`.

![](01.png)

- Dodaj następujące importy:

```csharp
using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;
```

- Dopisz w przestrzeni nazw następujący fragment:

```csharp
[InterfaceType(ComInterfaceType.InterfaceIsDual)]
public interface ICSSExplorerInterface
{
    void setButtonCaption(String strNewCaption);
    void setAdapterDllPtr(IntPtr i_AdapterDllPtr);
}

[ProgId("MyButton.MyButton")]
[ClassInterface(ClassInterfaceType.None)]
public partial class MyButton : UserControl, ICSSExplorerInterface
{
    private static uint DOT_NET_BUTTON_PRESSED = 0x0400;
    private IntPtr m_AdapterDllPtr;
    public MyButton()
    {
        InitializeComponent();
    }

    public void setButtonCaption(String strNewCaption)
    {
        btnOK.Text = strNewCaption;
    }

    public void setAdapterDllPtr(IntPtr i_AdapterDllPtr)
    {
        m_AdapterDllPtr = i_AdapterDllPtr;
    }   

    [ComRegisterFunction()]
    public static void RegisterClass(string i_Key)
    {
        StringBuilder sb = new StringBuilder(i_Key);
        sb.Replace(@"HKEY_CLASSES_ROOT\", "");
        RegistryKey registerKey = Registry.ClassesRoot.OpenSubKey(sb.ToString(), true);
        RegistryKey ctrl = registerKey.CreateSubKey("Control");
        ctrl.Close();
        RegistryKey inprocServer32 = registerKey.OpenSubKey("InprocServer32", true);
        inprocServer32.SetValue("CodeBase", Assembly.GetExecutingAssembly().CodeBase);
        inprocServer32.Close();
        registerKey.Close();
    }

    [ComUnregisterFunction()]
    public static void UnregisterClass(string i_Key)
    {
        StringBuilder sb = new StringBuilder(i_Key);
        sb.Replace(@"HKEY_CLASSES_ROOT\", "");
        RegistryKey registerKey = Registry.ClassesRoot.OpenSubKey(sb.ToString(), true);
        registerKey.DeleteSubKey("Control", false);
        RegistryKey inprocServer32 = registerKey.OpenSubKey("InprocServer32", true);
        inprocServer32.DeleteSubKey("CodeBase", false);
        registerKey.Close();
    }

    private void btnOK_Click(object sender, EventArgs e)
    {
        SendMessage(m_AdapterDllPtr.ToInt32(), DOT_NET_BUTTON_PRESSED, IntPtr.Zero, IntPtr.Zero);
    }

    #region MAPPING_OF_USER32_DLL_SECTION

    [DllImport("user32.dll", EntryPoint = "SendMessage")]
    public static extern IntPtr SendMessage(
        int hwnd, uint wMsg, IntPtr wParam, IntPtr lParam);

    [DllImport("user32.dll", EntryPoint = "SendMessage")]
    public static extern int SendMessage(
        int hwnd, uint wMsg, int wParam, string lParam);

    [DllImport("user32.dll", EntryPoint = "SendMessage")]
    public static extern int SendMessage(
        int hwnd, uint wMsg, int wParam, out int lParam);

    [DllImport("user32.dll", EntryPoint = "SendMessage")]
    public static extern int GetNbFiles(
        int hwnd, uint wMsg, int wParam, int lParam);

    [DllImport("user32.dll", EntryPoint = "SendMessage")]
    public static extern int GetFileNames(
        int hwnd, uint wMsg,
        [MarshalAs(UnmanagedType.LPArray)]IntPtr[] wParam,
        int lParam);

    [DllImport("user32.dll", EntryPoint = "SendMessage")]
    public static extern int SendMessage(
        int hwnd, uint wMsg, int wParam, StringBuilder lParam);

    #endregion
}
```

# Zmiany w projekcie CPP

- do pliku `Resource.h` należy dopisać następujące definicje:

```c++
#define IDM_BTN_CAP_HELLOWORLD          32774
#define IDM_BTN_CAP_OK                  32775
```

- dodać na sam początek polecenie importu pliku `.tlb`
```C++
#import "..\MyButton\bin\Debug\MyButton.tlb" named_guids raw_interfaces_only
```

Po tym pierwszym dopisaniu importa należy SKOMPILOWAĆ PROJEKT. W ten sposób utworzy się wymagany plik `.tlh`.

- dodać w deklaracjach:
```c++
#define DOT_NET_BUTTON_PRESSED  0x0400
HWND _hAtl;
HWND _hSelf;
IUnknown* _pUnk;
MyButton::ICSSExplorerInterfacePtr _pDotNetCOMPtr;
HINSTANCE _hWebLib = ::LoadLibrary(TEXT("ATL.DLL")); 
```

Następnie należy dodać funkcję `loadActiveX`:
```c++
void loadActiveX(LPCTSTR strActiveXName)
{
    BOOL (WINAPI *m_AtlAxWinInit)();
    m_AtlAxWinInit = (BOOL (WINAPI *)(void))::GetProcAddress(_hWebLib, "AtlAxWinInit");
    m_AtlAxWinInit();
    RECT rcClient; 
    GetClientRect(_hSelf, &rcClient); 
    _hAtl = ::CreateWindowEx(
    		WS_EX_CLIENTEDGE,\
    		TEXT("AtlAxWin"),\
    		strActiveXName,\
    		WS_CHILD | WS_VISIBLE | /*WS_CLIPCHILDREN | */WS_EX_RTLREADING,\
    		0, 0, rcClient.right, rcClient.bottom,\
    		_hSelf,\
    		NULL,\
    		NULL,\
    		NULL);

    if (!_hAtl)
    {
    	MessageBox( NULL, TEXT("Can not load AtlAxWin!"), 
				szTitle, MB_OK | MB_ICONSTOP);
    	throw int(106901);
    }

    HRESULT (WINAPI *m_AtlAxGetControl) (HWND h, IUnknown** pp);
    m_AtlAxGetControl = (HRESULT (WINAPI *) 
		(HWND, IUnknown**))::GetProcAddress(_hWebLib, "AtlAxGetControl");
    m_AtlAxGetControl(_hAtl, &_pUnk);
    _pUnk->QueryInterface(__uuidof(MyButton::ICSSExplorerInterface),
					(LPVOID *) &_pDotNetCOMPtr);
    if (_pDotNetCOMPtr != NULL)
    {
    	_pDotNetCOMPtr->setAdapterDllPtr((long) _hSelf);
    }
    else
    {
    	RECT rcClient; 
    	GetClientRect(_hSelf, &rcClient);

    	::DestroyWindow(_hAtl);
    	_hAtl = ::CreateWindowEx(
    				WS_EX_CLIENTEDGE,\
    				TEXT("AtlAxWin"),\
    				TEXT("MSHTML:""Please register ActiveX control before using this plugin."""),\
    				WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN | 
					WS_EX_RTLREADING,\
    				0, 0, rcClient.right, rcClient.bottom,\
    				_hSelf,\
    				NULL,\
    				NULL,\
    				NULL);
    }
}
```

Do funkcji wWinMain, tuż przed pętlą `while(GetMessage)` należy dodać:

```c++
loadActiveX(L"MyButton.MyButton");
```

W InitInstance, tuż przed `return TRUE` należy dopisać:

```c++
_hSelf = hWnd;
```

Na początku funkcji `WndProc` dopisać:

```c++
int wmId, wmEvent;
PAINTSTRUCT ps;
HDC hdc;
const char* strHelloWorld = "Hello World!";
const char* strOK = "OK";
_bstr_t bstrHelloWorld(strHelloWorld);
_bstr_t bstrOK(strOK);
```

Wewnątrz bloku `switch` przypadek `case WM_COMMAND` zamienić na:

```C++
case WM_COMMAND:
    {
        int wmId = LOWORD(wParam);
        // Parse the menu selections:
        switch (wmId)
        {
        case IDM_ABOUT:
            DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
            break;
        case IDM_EXIT:
            DestroyWindow(hWnd);
            break;
		case IDM_BTN_CAP_HELLOWORLD:
			_pDotNetCOMPtr->setButtonCaption(bstrHelloWorld);
			break;
		case IDM_BTN_CAP_OK:
			_pDotNetCOMPtr->setButtonCaption(bstrOK);
			break;
        default:
            return DefWindowProc(hWnd, message, wParam, lParam);
        }
    }
    break;
```

Zamienić końcówkę switcha na:

```c++
case WM_DESTROY:
	_pDotNetCOMPtr->Release();
	::DestroyWindow(_hAtl);
	_pUnk->Release();
	::FreeLibrary(_hWebLib);
    PostQuitMessage(0);
    break;
case DOT_NET_BUTTON_PRESSED:
	MessageBox(NULL, TEXT("Message from C# arrived: Button Pressed!!"), szTitle, MB_OK | MB_ICONINFORMATION);
	break;
default:
    return DefWindowProc(hWnd, message, wParam, lParam);
```

# Kontrolka WPF

Próbowałem dodać element WPF do projektu C++ w sposób analogiczny do WinForms. Bez powodzenia, najwyraźniej WinApi nie jest w stanie bezpośrednio renderować obiektów WPF.

Elementy WPF można dodawać tylko za pośrednictwem kontrolek WinForms i elementu `ElementHost`. 
Jak na razie efekt jest następujący:

![](02.png)

Mały biały przycisk jest kontrolką WinForms, wielki przycisk z czerwoną obramówką to element WPF.